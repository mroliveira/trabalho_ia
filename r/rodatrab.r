serieTS<-co2
source("trab.r")
write.table(resultados, "resultados_co2",row.names = FALSE,col.names = FALSE)


gap <- 300:320
serieTS[gap]<-NA
source("trab.r")
write.table(resultados, "resultados_co2_buracos",row.names = FALSE,col.names = FALSE)


ndata <- 1000
tt <- seq(0, 9, length=ndata)
meanf <- (sin(pi*tt) + sin(2*pi*tt) + sin(6*pi*tt)) * (0.0<tt & tt<=3.0) + (sin(pi*tt) + sin(6*pi*tt)) * (3.0<tt & tt<=6.0) + (sin(pi*tt) + sin(6*pi*tt) + sin(12*pi*tt)) * (6.0<tt & tt<=9.0)
snr <- 3.0
sigma <- c(sd(meanf[tt<=3]) / snr, sd(meanf[tt<=6 & tt>3]) / snr,
sd(meanf[tt>6]) / snr)
error <- c(rnorm(sum(tt<=3), 0, sigma[1]), rnorm(sum(tt<=6 & tt>3), 0, sigma[2]), rnorm(sum(tt>6), 0, sigma[3]))

serieTS <- meanf + error
source("trab.r")
write.table(resultados, "resultados_serieRuidosa",row.names = FALSE,col.names = FALSE)
